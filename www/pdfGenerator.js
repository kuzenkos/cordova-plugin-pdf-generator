var exec = require('cordova/exec');
function pdfGenerator() {}
pdfGenerator.prototype.htmlToPDF = function (options) {
    return new Promise(function (resolve, reject) {
        if (!url) {
            throw new Error("No URL or HTML Data found");
        }
        var url = options.url;
        var data = options.data;
        var docSize = options.documentSize || "A4";
        var landscape = options.landscape || "portrait";
        var type = options.type || "base64";
        var fileName = options.fileName || "default.pdf";
        var header = options.header || "";
        var footer = options.footer || "";
                       
        exec(resolve, reject, "PDFService", "htmlToPDF", [url, data, docSize, landscape, type, fileName, header, footer]);
    });
};
pdfGenerator.prototype.fromURL = function (url, options) {
    return new Promise(function (resolve, reject) {
        if (!url) {
            throw new Error("URL is required");
        }
        var data = null;
        var docSize = options.documentSize || "A4";
        var landscape = options.landscape || "portrait";
        var type = options.type || "base64";
        var fileName = options.fileName || "default.pdf";
        var header = options.header || "";
        var footer = options.footer || "";

        exec(resolve, reject, "PDFService", "htmlToPDF", [url, data, docSize, landscape, type, fileName, header, footer]);
    });
};
pdfGenerator.prototype.fromData = function (data, options) {
    return new Promise(function (resolve, reject) {
        if (!data) {
            throw new Error("String with HTML format is required");
        }
        var url = null;
        var docSize = options.documentSize || "A4";
        var landscape = options.landscape || "portrait";
        var type = options.type || "base64";
        var fileName = options.fileName || "default.pdf";
        var header = options.header || "";
        var footer = options.footer || "";
                       
        exec(resolve, reject, "PDFService", "htmlToPDF", [url, data, docSize, landscape, type, fileName, header, footer]);
    });
};
module.exports = new pdfGenerator();